def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'dockerhub-cred', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t devopstut01/react-app:1.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push devopstut01/react-app:1.0'
    }
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this
